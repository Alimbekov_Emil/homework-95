import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { FetchSoloCocktailRequest } from "../../store/actions/cocktailsActions";
import { Grid, Typography } from "@material-ui/core";
import imageNotAvailable from "../../assets/images/imageNotAvailable.png";
import { apiURL } from "../../config";

const SoloCocktail = (props) => {
  const dispatch = useDispatch();
  const cocktail = useSelector((state) => state.cocktails.cocktail);

  useEffect(() => {
    dispatch(FetchSoloCocktailRequest(props.match.params.id));
  }, [props.match.params.id, dispatch]);

  let cardImage = imageNotAvailable;

  if (cocktail.image) {
    cardImage = apiURL + "/" + cocktail.image;
  }

  return (
    <Grid container spacing={2} direction="column">
      {cocktail !== {} ? (
        <>
          <Typography variant="h3">{cocktail.title}</Typography>
          <Grid item container spacing={2}>
            <img src={cardImage} alt="cocktail" style={{ width: "400px", height: "300px" }} />
            <ul>
              <Typography variant="h5">Ingredients:</Typography>
              {cocktail.ingredients
                ? cocktail.ingredients.map((ing, index) => (
                    <li key={index} style={{ margin: " 5px 15px" }}>
                      {ing.title} {ing.amount}
                    </li>
                  ))
                : null}
            </ul>
          </Grid>
          <Grid item>
            <Typography variant="h6">Recipe:</Typography>
            <p>{cocktail.recipe}</p>
          </Grid>
        </>
      ) : null}
    </Grid>
  );
};

export default SoloCocktail;
