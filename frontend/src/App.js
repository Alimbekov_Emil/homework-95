import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AllCocktail from "./containers/AllCocktail/AllCocktail";
import AddNewCocktail from "./containers/AddNewCocktail/AddNewCocktail";
import MyCocktail from "./containers/MyCocktail/MyCocktail";
import SoloCocktail from "./containers/SoloCocktail/SoloCocktail";
import { useSelector } from "react-redux";

const ProtectedRoute = ({ isAllowed, redirectTo, ...props }) => {
  return isAllowed ? <Route {...props} /> : <Redirect to={redirectTo} />;
};

const App = () => {
  const user = useSelector((state) => state.users.user);
  return (
    <Layout>
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <ProtectedRoute path="/" exact component={AllCocktail} isAllowed={user} redirectTo="/login" />
        <ProtectedRoute
          path="/add-cocktail"
          component={AddNewCocktail}
          isAllowed={user}
          redirectTo="/login"
        />
        <ProtectedRoute path="/my-cocktail" component={MyCocktail} isAllowed={user} redirectTo="/login" />
        <Route path="/cocktail/:id" component={SoloCocktail} />
      </Switch>
    </Layout>
  );
};

export default App;
