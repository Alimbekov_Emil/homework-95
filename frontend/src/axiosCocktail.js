import axios from "axios";
import { apiURL } from "./config";

const axiosCocktail = axios.create({
  baseURL: apiURL,
});

export default axiosCocktail;
