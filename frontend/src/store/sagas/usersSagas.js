import { put, takeEvery } from "redux-saga/effects";
import {
  facebookLoginRequest,
  loginFailure,
  loginRequest,
  loginSuccess,
  logoutRequest,
  logoutSuccess,
  registerFailure,
  registerRequest,
  registerSuccess,
} from "../actions/usersActions";
import { historyPush } from "../actions/historyActions";
import API from "../../API";
import { addNotification } from "../actions/notifierActions";
import axiosCocktail from "../../axiosCocktail";

export function* registerUser({ payload: userData }) {
  try {
    const response = yield API.registerUser(userData);
    yield put(registerSuccess(response.data));
    yield put(historyPush("/"));
  } catch (error) {
    yield put(registerFailure(error.response.data));
  }
}

export function* loginUser({ payload: userData }) {
  try {
    const response = yield axiosCocktail.post("/users/sessions", userData);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush("/"));
    yield put(addNotification({ message: "Login successful", options: { variant: "success" } }));
  } catch (error) {
    yield put(loginFailure(error.response.data));
  }
}

export function* facebookLogin({ payload: data }) {
  try {
    const response = yield axiosCocktail.post("/users/facebookLogin", data);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush("/"));
    yield put(addNotification({ message: "Login successful", options: { variant: "success" } }));
  } catch (error) {
    yield put(loginFailure(error.response.data));
  }
}

export function* logout() {
  try {
    yield axiosCocktail.delete("/users/sessions");
    yield put(logoutSuccess());
    yield put(historyPush("/"));
    yield put(addNotification({ message: "Logged out", options: { variant: "success" } }));
  } catch (e) {
    yield put(addNotification({ message: "Could not logout", options: { variant: "error" } }));
  }
}

const usersSagas = [
  takeEvery(registerRequest, registerUser),
  takeEvery(loginRequest, loginUser),
  takeEvery(facebookLoginRequest, facebookLogin),
  takeEvery(logoutRequest, logout),
];

export default usersSagas;
