import { combineReducers } from "redux";

import usersSlice from "./slices/usersSlice";
import notifierSlice from "./slices/notifierSlice";
import cocktailsSlice from "./slices/coktailsSlice";

const rootReducer = combineReducers({
  users: usersSlice.reducer,
  cocktails: cocktailsSlice.reducer,
  notifier: notifierSlice.reducer,
});

export default rootReducer;
