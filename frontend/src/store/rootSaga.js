import { all } from "redux-saga/effects";
import history from "../history";
import cocktailsSagas from "./sagas/cocktailsSagas";
import historySagas from "./sagas/historySagas";
import usersSagas from "./sagas/usersSagas";

export default function* rootSaga() {
  yield all([...historySagas(history), ...usersSagas, ...cocktailsSagas]);
}
