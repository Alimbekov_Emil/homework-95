const mongoose = require("mongoose");
const config = require("./config");
const { nanoid } = require("nanoid");
const User = require("./models/User");
const { create } = require("./models/User");
const Cocktail = require("./models/Cocktail");

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create(
    {
      email: "emil@mail.ru",
      password: "12345",
      token: nanoid(),
      role: "user",
      displayName: "Эмиль",
    },
    {
      email: "admin@mail.ru",
      password: "12345",
      token: nanoid(),
      role: "admin",
      displayName: "Администратор",
    }
  );

  await Cocktail.create(
    {
      title: "Маргарита",
      image: "fixtures/margarita.jpeg",
      user: user,
      published: true,
      role: "user",
      recipe:
        "Сделай на бокале для маргариты соленую окаемку Налей в шейкер лаймовый сок 30 мл, сахарный сироп 10 мл, ликер трипл сек 25 мл и серебряную текилу 50 мл Наполни шейкер кубиками льда и взбей Перелей через стрейнер в охлажденный бокал для маргариты Укрась кружком лайма",
      ingredients: [
        { title: "Лайм", amount: "10мл" },
        { title: "Cоль", amount: "2г" },
        { title: "Сахарный Сироп", amount: "10мл" },
        { title: "Серебреная Текилла", amount: "50мл" },
        { title: "Трипл сек de Kuyper", amount: "10мл" },
      ],
    },
    {
      title: "Кровавая мэри",
      image: "fixtures/bloody-mary.jpeg",
      user: admin,
      published: true,
      role: "admin",
      recipe:
        "В охлаждённый бокал хайболл наливаем сначала томатный сок, затем аккуратно по барной ложечке наливаем водку, чтобы она не смешалась с соком. Солим, перчим, капаем острый соус и вустерский соус. Украшаем стеблем сельдерея.",
      ingredients: [
        { title: "Томатный сок", amount: "150мл" },
        { title: "Водка", amount: "40мл" },
        { title: "Острый соус", amount: "10мл" },
      ],
    }
  );

  await mongoose.connection.close();
};

run().catch(console.error);
