const path = require("path");
const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, "public"),
  db: {
    url: "mongodb://localhost/cocktailApi",
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    },
  },
  facebook: {
    appId: "1337978219922706",
    appSecret: "6dade69c8af9cfb317fc6d0f1e750382",
  },
};
