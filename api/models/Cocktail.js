const mongoose = require("mongoose");

const IngredienSchema = new mongoose.Schema({
  title: String,
  amount: String,
});

const CocktailSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  image: String,
  recipe: {
    type: String,
    required: true,
  },
  published: {
    type: String,
    required: true,
    default: false,
    enum: [true, false],
  },
  ingredients: [IngredienSchema],
});

const Cocktail = mongoose.model("Cocktail", CocktailSchema);
module.exports = Cocktail;
